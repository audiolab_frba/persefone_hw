# Motivaci�n #
Acercar herramientas de software, funciones y scripts en diferentes lenguajes.
___
# Versi�n 2.0 #
Se busca una simplificaci�n sin perder funcionalidades, atacando los errores de dise�o y aquello que haya resultado contraproducente.

* Alimentaci�n con fuente simple
* Posibilidad de alimentaci�n USB
* Codec de 24bits hasta 192kHz de muestreo
* Simplificaci�n del Front End
* Conectores de audio robustos
___
# Versi�n 1.0#

| `Propiedades` |
| --------------|
| incluye todo lo necesario para un desarrollo DSP en frecuencia de audio |
| provee entrada y salida mono |
| pulsadores |
| pantalla TFT |
| USB nativo |

| `Fallas` |
| --------------|
| footprints de operacionales con uno de ellos mal |
| potenciometros conectados al reves |
| potensiometro SPI de salida fall� |
| el bridge usb-serie no pudo ser probado | 
