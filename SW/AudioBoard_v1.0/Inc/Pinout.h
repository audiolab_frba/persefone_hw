#ifndef PINOUT_H
#define PINOUT_H


#include "stm32f4xx_hal.h"


#define 	PIN_LED_R		GPIOF, GPIO_PIN_11
#define 	PIN_LED_G		GPIOB, GPIO_PIN_2
#define 	PIN_LED_B		GPIOB, GPIO_PIN_1


#endif // PINOUT_H
